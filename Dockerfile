FROM node:14.4.0-alpine

RUN npm install -g http-server

WORKDIR /app

# COPY package*.json ./

# RUN npm install

COPY ./dist ./dist

EXPOSE 8080

CMD [ "http-server", "dist" ]
